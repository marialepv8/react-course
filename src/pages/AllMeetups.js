import MeetupList from "../components/meetups/MeetupList";

function AllMeetupsPage() {
  // fetch('https://react-course-7a095-default-rtdb.firebaseio.com/').then(response => {
  //   return response.json();
  // }).then(data => {

  // })

  return (
      <section>
          <h1>All Meetups</h1>
          <MeetupList meetups={DUMMY_DATA}/>
      </section>
  )
}

export default AllMeetupsPage
